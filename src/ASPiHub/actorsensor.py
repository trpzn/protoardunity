import logging
import time
from threading import Thread

from SerialCommunication import SerialCommunication
from hdlcnocontrol import HdlcNoControl

"""
    1) instantiate object
    2) connect to device
    3) send message to retrieve device id
"""
class ActorSensor(Thread):
    baud_rate = 115200

    seek_device_max_time = 0.5
    #7E 01 01 05 9A 2D 7E
    seek_device_id_command = [0x7E,0x01,0x01,0x05,0x9A,0x2D,0x7E]
    turn_on_pairing_ligth_command = []

    def __init__(self, hub_ref, com_port):
        super(ActorSensor, self).__init__(name='actorsensor')

        self.hub = None
        self.is_running = True
        self.com_port_used = None
        self.connection_enabled = False
        self.hdlc = None
        self.serial_connection = None

        self.seek_device_max_attempts = 2
        self.seek_device_attempts = 0
        self.seek_device_waiting_response = False
        self.seek_device_start_time = 0

        self.seeking_device_id = True

        self.hub = hub_ref
        self.com_port_used = com_port
        self.hdlc = HdlcNoControl(
            self.__send_byte_to_device,
            self.__received_frame_from_device,
            no_crop=True
        )
        self.serial_communication = SerialCommunication(
            self,
            com_port,
            self.baud_rate
        )

        if self.serial_communication.error:
            self.hub.sensor_actor_com_port_error(self)
            return
        else:
            self.hub.set_com_busy(com_port,self)
            self.start()
        """
        self.hub.set_com_busy(com_port)
        self.start()
        """

    def run(self):
        while self.is_running:
            # Read bytes received in com port
            #self.__read_byte_from_serial_buffer()
            #self.__check_byte_from_serial()
            if not self.connection_enabled:
                if self.seeking_device_id:
                    self.__command_get_device_id()
                else:
                    #  no se esta buscando al dispositivo
                    pass
            else:
                pass
        return

    def end_run(self):
        pass

    def is_com_port(self,com_port):
        if self.com_port_used == com_port:
            return True
        else:
            return False

    def __send_byte_to_device(self,byte):
        self.serial_communication.write(byte)

    def __received_byte_from_device(self,byte):
        self.hdlc.char_receiver(byte)

    def __check_byte_from_serial(self):
        byte = self.serial_communication.read()
        if byte:
            self.hdlc.char_receiver(byte)

    """Frame detected form device"""
    def __received_frame_from_device(self,frame,id):
        #print(frame)
        logging.debug('ac     -> device ['+ str(id) + '] :'+ str(frame))
        if self.seek_device_waiting_response:
            self.__seek_device_id_in_response(frame)
        else:
            #self.hub.message_to_device(frame)
            self.hub.packets_to_device.put_nowait(frame)
            #self.hub.

    """Frame detected from hub"""
    def received_from_hub(self,frame):
        #self.__received_from_hub(frame)
        self.serial_communication.write(frame)

    def __received_from_hub(self,frame):
        for byte in frame:
            self.__send_byte_to_device(byte)

    seek_device_id_seconds_w8ting = 0

    def __command_get_device_id(self):
        if not self.seek_device_waiting_response:
            if self.seek_device_attempts <= self.seek_device_max_attempts:
                #print("Seeking id")
                self.seek_device_start_time = time.time()
                self.seek_device_attempts += 1
                self.seek_device_waiting_response = True
                self.serial_communication.write(self.seek_device_id_command)
            else:
                logging.info("[AS] [port: "+ self.com_port_used
                     + "] [Not an AS device]")
                self.seek_device_waiting_response = False
                self.seeking_device_id = False
                self.connection_enabled = False
                #  max attempts throw error
        else:
            elapsed_time = time.time() - self.seek_device_start_time
            if elapsed_time >= self.seek_device_max_time:
                self.seek_device_waiting_response = False
            # waiting for a response from device

    def __seek_device_id_in_response(self,response):
        """
        Seek for device id in response package of actorsensor device
        :param response:
        :return:
        """
        # int[] response
        direction = response[1]
        command_type = response[2]
        command_id = response[3]
        device_id = response[4]
        #print(response)

        if command_type == 2 and command_id == 5:
            # response received
            # print(device_id)
            self.seek_device_waiting_response = False
            self.seeking_device_id = False
            self.connection_enabled = True

            #
            # Seek for the device id in devices list
            # if don't exist add to list and turn on pairing light of device
            # if exist delete this device

            # look for the device id in devices list
            device_ref = self.hub.get_sensor_actor_by_id(device_id)
            #device_ref = self.hub.devicesList[device_id]
            if device_ref is None:
                self.hub.add_to_device_list_by_id(device_id,self)
                #self.hub.devicesList[device_id] = self
                self.__turn_on_pairing_light()
            else:
                self.is_running = False

    def __turn_on_pairing_light(self):
        pass

    def put_data_from_serial(self, byte):
        #print(byte)
        for bit in byte:
            self.hdlc.char_receiver(bit)

    def serial_disconnected(self):
        self.is_running = False
        self.serial_communication.stop()
        logging.info("[AS] [port: " + self.com_port_used
                     + "] [Disconnected]")
        self.hub.set_com_free(self.com_port_used)
