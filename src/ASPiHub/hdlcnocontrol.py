from crcCcitt import *
"""
//PACKAGE EXAMPLE

//---FRAME_BOUNDARY---//----DIRECTION----//----DATA----//----CRC----//----FRAME_BOUNDARY----//
//-----1BYTE----------//-----1BYTE-------//----nBYTE---//---2BYTE---//-----1BYTE------------//


//----------------------DATA-----------------//
//----DATA_TYPE---//--COMMAND--//----DATA----//
//------1BYTE-----//---1BYTE---//---NBYTES---//

"""

class HdlcNoControl:
    FRAME_BOUNDARY_OCTET = 0x7E
    CONTROL_ESCAPE_OCTET = 0x7D
    INVERT_OCTET = 0x20
    CRC16_CCITT_INIT_VAL = 0xFFFF
    CRC_LENGTH = 2
    FRAME_BOUNDARY_LENGTH =1
    ADDRESS_LENGTH = 1
    MIN_REQ_DATA = 4
    max_frame_length = 32

    def __init__(self, send_char_function, frame_handler_function, no_crop = False):
        self.frame_position = 0
        self.receive_frame_buffer = list()
        self.escape_character = False
        self.frame_checksum = self.CRC16_CCITT_INIT_VAL
        self.send_char_function = send_char_function
        self.frame_handler_function = frame_handler_function
        self.no_crop = no_crop

    def char_receiver(self,data):
        #print(data)
        #cast incomming data for garbage bits
        data = crcCcitt.uint8_t(data)

        if data == self.FRAME_BOUNDARY_OCTET:
            if self.escape_character:
                self.escape_character = False
            elif len(self.receive_frame_buffer) >= 2 and self.frame_checksum == (
                        (self.receive_frame_buffer[len(self.receive_frame_buffer) - 1] << 8) |
                        (self.receive_frame_buffer[len(self.receive_frame_buffer) - 2] & 0xff)
            ):
                ####return detected frame
                ##self.__frame_detected(list(self.frame_position))
                ##print(self.receive_frame_buffer)
                if len(self.receive_frame_buffer) > self.MIN_REQ_DATA:
                    address = self.receive_frame_buffer[0]
                    if self.no_crop:
                        new_buffer = [self.FRAME_BOUNDARY_OCTET] + list(self.receive_frame_buffer) + [self.FRAME_BOUNDARY_OCTET]
                        self.receive_frame_buffer = new_buffer
                        address = self.receive_frame_buffer[1]
                    buffer = self.receive_frame_buffer
                    self.receive_frame_buffer = list()
                    self.frame_handler_function(buffer,address)
                else:
                    self.clear_buffer()
                ###
            self.frame_position = 0
            self.frame_checksum = self.CRC16_CCITT_INIT_VAL
            return
        if self.escape_character:
            self.escape_character = False
            data ^= self.INVERT_OCTET
        elif data == self.CONTROL_ESCAPE_OCTET:
            self.escape_character = True;
            return

        self.receive_frame_buffer.append(data)
        if len(self.receive_frame_buffer) - 3 >= 0:
            self.frame_checksum = crcCcitt.crc_ccitt_update(
                self.frame_checksum,
                self.receive_frame_buffer[len(self.receive_frame_buffer) - 3]
            )

        if len(self.receive_frame_buffer) == self.max_frame_length:
            self.clear_buffer();
            self.frame_checksum = self.CRC16_CCITT_INIT_VAL

    def frame_sender(self,frame_to_send,no_crop=False):
        if no_crop:
            for data in frame_to_send:
                self.send_char_function(data)
            return
        else:
            return

    def clear_buffer(self):
        self.receive_frame_buffer = list()





