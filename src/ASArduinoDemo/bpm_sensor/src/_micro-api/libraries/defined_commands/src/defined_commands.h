// defined_commands.h

#ifndef _DEFINED_COMMANDS_h
#define _DEFINED_COMMANDS_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

enum serial_data_types {
	DATA_TYPE_ERROR,
	DATA_TYPE_COMMAND,
	DATA_TYPE_RESPONSE
};

enum serial_commands {
	COMMAND_ERROR = 0, // 0
	COMMAND_SET_SERVO_POSITION,
	COMMAND_TOGGLE_LED,
	COMMAND_READ_SIGNATURE,
	COMMAND_ECHO_DATA,
	COMMAND_GET_ID,
	COMMAND_ONOFF_DEVICE,
	COMMAND_SET_FAN_SPEED
};

enum serial_responses {
	RESPONSE_ERROR = 0, // 0
	RESPONSE_VERSION,
	RESPONSE_BUTTON_PRESS,
	RESPONSE_READ_SIGNATURE,
	RESPONSE_ECHO_DATA,
	RESPONSE_ECHO_ID,
	RESPONSE_ONOFF_DEVICE
};

struct hdlc_frame {
	serial_data_types dataType;
	serial_commands commandType;
	serial_responses responseType;
	uint8_t *data;
	uint8_t datalength;
};


/*
turn 0 the vent
7E 02 01 07 00 83 2D 7E

turn 118 the vent
7E 02 01 07 2F 76 F4 7E

turn 255 the vent
7E 02 01 07 FF FB 22 7E

turn on device
7E 02 01 06 01 D2 25 7E


*/
#endif

