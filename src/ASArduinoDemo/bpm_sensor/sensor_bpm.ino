/*
Name:		Sketch1.ino
Created:	19/05/2016 23:02:58
Author:	Trpzn
*/


#define DEBUG

#include "defined_commands.h"
#include "Arduhdlc.h"
#include <avr/boot.h>


////////////////////////////////////////////////////
/// Sensor variables
/* How long (bytes) is the longest HDLC frame? */
#define MAX_HDLC_FRAME_LENGTH 32
//#define SERVO_PIN 14
#define LED_ON_PIN 7

#define VERSION_NUMBER 0.1
#define DEVICE_UNIQUE_ADDRESS 3
#define RECEIVER_UNIQUE_ADRESS 3
#define SERIAL_BAUD_RATE 115200

/* Variable for LED toggle */
boolean toggle = false;

/// End Sensor variables
/////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/// Pulsesensor Variables
int pulsePin = 0;                 // Pulse Sensor purple wire connected to analog pin 0
int blinkPin = 13;                // pin to blink led at each beat
int fadePin = 5;                  // pin to do fancy classy fading blink at each beat
int fadeRate = 0;                 // used to fade LED on with PWM on fadePin

								  // Volatile Variables, used in the interrupt service routine!
volatile int BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded! 
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat". 
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.

									// Regards Serial OutPut  -- Set This Up to your needs
static boolean serialVisual = true;   // Set to 'false' by Default.  Re-set to 'true' to see Arduino Serial Monitor ASCII Visual Pulse 
/// end pulsesensor Variables
/////////////////////////////////////////////////////

/* Function to send out byte/char */
void send_character(uint8_t data, uint8_t type);
/* Function to handle a valid HDLC frame */
void hdlc_command_router(const uint8_t *framebuffer, uint16_t framelength, uint8_t address);
void commands_router(hdlc_frame *frame);

/* Command execute functions for each command */
void command_error(void);
void command_get_id(void);

/*Status of the device for automatic send data*/
int device_status = 0;

boolean buttonstatus = false;
unsigned long time;
unsigned long elapsedTime = 0;
unsigned long waitingTimeMS = 1;

/* Initialize Arduhdlc library with three parameters.
1. Character send function, to send out HDLC frame one byte at a time.
2. HDLC frame handler function for received frame.
3. Length of the longest frame used, to allocate buffer in memory */
Arduhdlc hdlc(&send_character, &hdlc_command_router, MAX_HDLC_FRAME_LENGTH, DEVICE_UNIQUE_ADDRESS);


/////////////////////////////////////////////////////////////////////////////////////

//////////
/////////  All Serial Handling Code, 
/////////  It's Changeable with the 'serialVisual' variable
/////////  Set it to 'true' or 'false' when it's declared at start of code.  
/////////

void serialOutput() {   // Decide How To Output Serial. 
	if (serialVisual == true) {
		//arduinoSerialMonitorVisual('-', Signal);   // goes to function that makes Serial Monitor Visualizer
	}
	else {
		//sendDataToSerial('S', Signal);     // goes to sendDataToSerial function
	}
}


//  Decides How To OutPut BPM and IBI Data
void serialOutputWhenBeatHappens() {
	if (serialVisual == true) {            //  Code to Make the Serial Monitor Visualizer Work
		//Serial.print(BPM);
		if (device_status) {
			uint8_t data[] = { (uint8_t)2,(uint8_t)4,(uint8_t)BPM };
			if (BPM > 120 || BPM < 40) {
				data[2] = 0;
			}
			else {
				data[2] = BPM;
			}
			hdlc.frameDecode(data, sizeof(data), DEVICE_UNIQUE_ADDRESS);
		}

		
	}
	else {
		Serial.println(BPM);
		//sendDataToSerial('B', BPM);   // send heart rate with a 'B' prefix
		//sendDataToSerial('Q', IBI);   // send time between beats with a 'Q' prefix
	}
}



//  Sends Data to Pulse Sensor Processing App, Native Mac App, or Third-party Serial Readers. 
void sendDataToSerial(char symbol, int data) {
	Serial.print(symbol);

	Serial.println(data);
}


//  Code to Make the Serial Monitor Visualizer Work
void arduinoSerialMonitorVisual(char symbol, int data) {
	const int sensorMin = 0;      // sensor minimum, discovered through experiment
	const int sensorMax = 1024;    // sensor maximum, discovered through experiment

	int sensorReading = data;
	// map the sensor range to a range of 12 options:
	int range = map(sensorReading, sensorMin, sensorMax, 0, 11);

	// do something different depending on the 
	// range value:
	switch (range) {
	case 0:
		Serial.println("");     /////ASCII Art Madness
		break;
	case 1:
		Serial.println("---");
		break;
	case 2:
		Serial.println("------");
		break;
	case 3:
		Serial.println("---------");
		break;
	case 4:
		Serial.println("------------");
		break;
	case 5:
		Serial.println("--------------|-");
		break;
	case 6:
		Serial.println("--------------|---");
		break;
	case 7:
		Serial.println("--------------|-------");
		break;
	case 8:
		Serial.println("--------------|----------");
		break;
	case 9:
		Serial.println("--------------|----------------");
		break;
	case 10:
		Serial.println("--------------|-------------------");
		break;
	case 11:
		Serial.println("--------------|-----------------------");
		break;

	}
}

/////////////////////////////////////////////////////////////////////////////////////

volatile int rate[10];                    // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0;          // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;           // used to find IBI
volatile int P = 512;                      // used to find peak in pulse wave, seeded
volatile int T = 512;                     // used to find trough in pulse wave, seeded
volatile int thresh = 525;                // used to find instant moment of heart beat, seeded
volatile int amp = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;        // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;      // used to seed rate array so we startup with reasonable BPM


void interruptSetup() {
	// Initializes Timer2 to throw an interrupt every 2mS.
	TCCR2A = 0x02;     // DISABLE PWM ON DIGITAL PINS 3 AND 11, AND GO INTO CTC MODE
	TCCR2B = 0x06;     // DON'T FORCE COMPARE, 256 PRESCALER 
	OCR2A = 0X7C;      // SET THE TOP OF THE COUNT TO 124 FOR 500Hz SAMPLE RATE
	TIMSK2 = 0x02;     // ENABLE INTERRUPT ON MATCH BETWEEN TIMER2 AND OCR2A
	sei();             // MAKE SURE GLOBAL INTERRUPTS ARE ENABLED      
}


// THIS IS THE TIMER 2 INTERRUPT SERVICE ROUTINE. 
// Timer 2 makes sure that we take a reading every 2 miliseconds
ISR(TIMER2_COMPA_vect) {                         // triggered when Timer2 counts to 124
	if (!device_status) return;
	cli();                                      // disable interrupts while we do this
	Signal = analogRead(pulsePin);              // read the Pulse Sensor 
	sampleCounter += 2;                         // keep track of the time in mS with this variable
	int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

												//  find the peak and trough of the pulse wave
	if (Signal < thresh && N >(IBI / 5) * 3) {       // avoid dichrotic noise by waiting 3/5 of last IBI
		if (Signal < T) {                        // T is the trough
			T = Signal;                         // keep track of lowest point in pulse wave 
		}
	}

	if (Signal > thresh && Signal > P) {          // thresh condition helps avoid noise
		P = Signal;                             // P is the peak
	}                                        // keep track of highest point in pulse wave

											 //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
											 // signal surges up in value every time there is a pulse
	if (N > 250) {                                   // avoid high frequency noise
		if ((Signal > thresh) && (Pulse == false) && (N > (IBI / 5) * 3)) {
			Pulse = true;                               // set the Pulse flag when we think there is a pulse
			digitalWrite(blinkPin, HIGH);                // turn on pin 13 LED
			IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
			lastBeatTime = sampleCounter;               // keep track of time for next pulse

			if (secondBeat) {                        // if this is the second beat, if secondBeat == TRUE
				secondBeat = false;                  // clear secondBeat flag
				for (int i = 0; i <= 9; i++) {             // seed the running total to get a realisitic BPM at startup
					rate[i] = IBI;
				}
			}

			if (firstBeat) {                         // if it's the first time we found a beat, if firstBeat == TRUE
				firstBeat = false;                   // clear firstBeat flag
				secondBeat = true;                   // set the second beat flag
				sei();                               // enable interrupts again
				return;                              // IBI value is unreliable so discard it
			}


			// keep a running total of the last 10 IBI values
			word runningTotal = 0;                  // clear the runningTotal variable    

			for (int i = 0; i <= 8; i++) {                // shift data in the rate array
				rate[i] = rate[i + 1];                  // and drop the oldest IBI value 
				runningTotal += rate[i];              // add up the 9 oldest IBI values
			}

			rate[9] = IBI;                          // add the latest IBI to the rate array
			runningTotal += rate[9];                // add the latest IBI to runningTotal
			runningTotal /= 10;                     // average the last 10 IBI values 
			BPM = 60000 / runningTotal;               // how many beats can fit into a minute? that's BPM!
			QS = true;                              // set Quantified Self flag 
													// QS FLAG IS NOT CLEARED INSIDE THIS ISR
		}
	}

	if (Signal < thresh && Pulse == true) {   // when the values are going down, the beat is over
		digitalWrite(blinkPin, LOW);            // turn off pin 13 LED
		Pulse = false;                         // reset the Pulse flag so we can do it again
		amp = P - T;                           // get amplitude of the pulse wave
		thresh = amp / 2 + T;                    // set thresh at 50% of the amplitude
		P = thresh;                            // reset these for next time
		T = thresh;
	}

	if (N > 2500) {                           // if 2.5 seconds go by without a beat
		thresh = 512;                          // set thresh default
		P = 512;                               // set P default
		T = 512;                               // set T default
		lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date        
		firstBeat = true;                      // set these to avoid noise
		secondBeat = false;                    // when we get the heartbeat back
	}

	sei();                                   // enable interrupts when youre done!
}// end isr

///////////////////////////////////////////////////////////////////////////////////////



/* Function to send out one 8bit character */
void send_character(uint8_t data, uint8_t type) {
	if (type == 0) {
		Serial.write(data);
		//Serial.print('_');
	}
	else {
		Serial.print(data, type);
	}
}


void command_toggle_led(const uint8_t *framebuffer, uint16_t framelengthd) {
	toggle = !toggle;
	digitalWrite(LED_ON_PIN, toggle);
}

// ECHO the data back, for testing CRC sum
void command_echo_data(uint8_t *framebuffer, uint16_t framelength) {
	hdlc.frameDecode(framebuffer, framelength, RECEIVER_UNIQUE_ADRESS);
}

void command_error(void) {
	uint8_t data[2] = { (uint8_t)RESPONSE_ERROR, 1 };
	data[0] = RESPONSE_ERROR;
	Serial.write("Error");
	hdlc.frameDecode(data, 2, RECEIVER_UNIQUE_ADRESS);
}

void command_default(void) {
	Serial.write("default");
}

hdlc_frame *fillFrame(const uint8_t *framebuffer, uint16_t framelength) {
	hdlc_frame *frame = (hdlc_frame *)malloc(sizeof(hdlc_frame));
	/*Serial.print('\n');
	for (int x = 0; x < framelength; ++x) {
	Serial.print(framebuffer[x], HEX);
	Serial.write(' ');
	}
	Serial.print('\n');*/
	//return frame;
	frame->dataType = static_cast<serial_data_types>(framebuffer[0]);
	if (frame->dataType == DATA_TYPE_COMMAND) {
		frame->responseType = RESPONSE_ERROR;
		frame->commandType = static_cast<serial_commands>(framebuffer[1]);
	}
	else if (frame->dataType == DATA_TYPE_RESPONSE) {
		frame->responseType = static_cast<serial_responses>(framebuffer[1]);
		frame->commandType = COMMAND_ERROR;
	}
	frame->datalength = framelength - 2;
	frame->data = (uint8_t*)malloc(sizeof(uint8_t)*(framelength - 2));
	for (int x = 2; x < framelength; ++x) {
		frame->data[x - 2] = framebuffer[x];
	}
	return frame;
}


/*
* Command router/dispatcher
* Calls the right command, passing data to it.
* @TODO Make it better
* https://doanduyhai.wordpress.com/2012/08/04/design-pattern-the-asynchronous-dispatcher/
*/
void hdlc_command_router(const uint8_t *framebuffer, uint16_t framelength, uint8_t address) {
	//Serial.println("Hola");
	hdlc_frame *frame = fillFrame(framebuffer, framelength);
	//Serial.println(frame->dataType,HEX);
	//return;
	//enum serial_data_types datatype = static_cast<serial_data_types>(framebuffer[0]);
	switch (frame->dataType)
	{
	case DATA_TYPE_ERROR:  break;
	case DATA_TYPE_COMMAND: commands_router(frame); break;
	case DATA_TYPE_RESPONSE: responses_router(frame); break;
	default:
		delete frame;
		data_type_default();
		break;
	}

}

void data_type_default() {

}

void commands_router(hdlc_frame *frame) {
	//Serial.println(frame->commandType);
	//Serial.print(frame->commandType);
	switch (frame->commandType)
	{
		//case COMMAND_ERROR:                  command_error(); break;
	case COMMAND_GET_ID:				 command_get_id(); delete frame; break;
	case COMMAND_ONOFF_DEVICE:              command_onoff_device(frame); break;
	default:
		delete frame;
		command_default();
		break;
	}
}

void command_get_id() {
	uint8_t data[3];
	data[0] = DATA_TYPE_RESPONSE;
	data[1] = RESPONSE_ECHO_ID; //Device Signature Byte 1
	data[2] = DEVICE_UNIQUE_ADDRESS; //Device Signature Byte 2
	hdlc.frameDecode(data, 3, RECEIVER_UNIQUE_ADRESS);
}

void command_onoff_device(hdlc_frame *frame) {

	uint8_t data[3];

	if (frame->data[0] == 1 && device_status == 0) {
		//turn on the device
		device_status = 1;
		digitalWrite(LED_ON_PIN, HIGH);
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 1;

	}
	else if (frame->data[0] == 0 && device_status == 1) {
		device_status = 0;
		digitalWrite(LED_ON_PIN, LOW);
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 0;
	}
	else {
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 2; //error

	}
	hdlc.frameDecode(data, 3, RECEIVER_UNIQUE_ADRESS);
	delete frame;
}


void responses_router(hdlc_frame *frame) {
	switch (frame->commandType)
	{
		//case COMMAND_ERROR:                  command_error(); break;
		//case COMMAND_GET_ID:				 command_get_id(framebuffer, framelength); break;
		//case COMMAND_TOGGLE_ON:       command_toggle_led(framebuffer, framelength); break;
	default:
		command_default();
		break;
	}
}


void ledFadeToBeat() {
	fadeRate -= 15;                         //  set LED fade value
	fadeRate = constrain(fadeRate, 0, 255);   //  keep LED fade value from going into negative numbers!
	analogWrite(fadePin, fadeRate);          //  fade LED
}

void setup() {

	pinMode(blinkPin, OUTPUT);         // pin that will blink to your heartbeat!
	pinMode(fadePin, OUTPUT);

	pinMode(LED_ON_PIN, OUTPUT);
	
	Serial.begin(SERIAL_BAUD_RATE);

	interruptSetup();

}

void loop() {

	time = millis();
	if ((time - elapsedTime) >= waitingTimeMS) {
		//checkButtonPressed();
		elapsedTime = time;
	}
	if (device_status) {
		//Serial.print(1);
		serialOutput();
		if (QS == true) {     // A Heartbeat Was Found
							  // BPM and IBI have been Determined
							  // Quantified Self "QS" true when arduino finds a heartbeat
			fadeRate = 255;         // Makes the LED Fade Effect Happen
									// Set 'fadeRate' Variable to 255 to fade LED with pulse
			serialOutputWhenBeatHappens();   // A Beat Happened, Output that to serial.     
			QS = false;                      // reset the Quantified Self flag for next time    
		}
		if (BPM>120 || BPM<40) {
			//digitalWrite(fadePin,true);
			digitalWrite(blinkPin, HIGH);
		}
		else {
			//digitalWrite(fadePin,false);
			ledFadeToBeat();                      // Makes the LED Fade Effect Happen 
		}
	}
	
	delay(20);                             //  take a break

}



/*
SerialEvent occurs whenever a new data comes in the
hardware serial RX.  This routine is run between each
time loop() runs, so using delay inside loop can delay
response.  Multiple bytes of data may be available.
*/
void serialEvent() {
	int incomingByte = 0;
	while (Serial.available()) {
		uint8_t inChar = (uint8_t)Serial.read();
		hdlc.charReceiver(inChar);

	}
}