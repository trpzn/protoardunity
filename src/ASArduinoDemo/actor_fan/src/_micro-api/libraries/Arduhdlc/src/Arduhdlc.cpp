#include "Arduino.h"
#include "Arduhdlc.h"


/*
//PACKAGE EXAMPLE

//---FRAME_BOUNDARY---//----DIRECTION----//----DATA----//----CRC----//----FRAME_BOUNDARY----//
//-----1BYTE----------//-----1BYTE-------//----nBYTE---//---2BYTE---//-----1BYTE------------//


//----------------------DATA-----------------//
//----DATA_TYPE---//--COMMAND--//----DATA----//
//------1BYTE-----//---1BYTE---//---NBYTES---//

*/



#define DEBUG

/* HDLC Asynchronous framing */
/* The frame boundary octet is 01111110, (7E in hexadecimal notation) */
#define FRAME_BOUNDARY_OCTET 0x7E

/* A "control escape octet", has the bit sequence '01111101', (7D hexadecimal) */
#define CONTROL_ESCAPE_OCTET 0x7D

/* If either of these two octets appears in the transmitted data, an escape octet is sent, */
/* followed by the original data octet with bit 5 inverted */
#define INVERT_OCTET 0x20

/* The frame check sequence (FCS) is a 16-bit CRC-CCITT */
/* AVR Libc CRC function is _crc_ccitt_update() */
/* Corresponding CRC function in Qt (www.qt.io) is qChecksum() */
#define CRC16_CCITT_INIT_VAL 0xFFFF

#define CRC_LENGTH 2
#define FRAME_BOUNDARY_LENGTH 1
#define ADDRESS_LENGTH 1

/* 16bit low and high bytes copier */
#define low(x)    ((x) & 0xFF)
#define high(x)   (((x)>>8) & 0xFF)

Arduhdlc::Arduhdlc(sendchar_type put_char, frame_handler_type hdlc_command_router, uint16_t max_frame_length, uint16_t device_address) : sendchar_function(put_char), frame_handler(hdlc_command_router)
{
	this->device_address = device_address;
	this->frame_position = 0;
	this->max_frame_length = max_frame_length;
	this->receive_frame_buffer = (uint8_t *)malloc(max_frame_length + 1); // char *ab = (char*)malloc(12);
	this->frame_checksum = CRC16_CCITT_INIT_VAL;
	this->escape_character = false;
}

/* Function to send a byte throug USART, I2C, SPI etc.*/
void Arduhdlc::sendchar(uint8_t data,uint8_t type)
{
	(*this->sendchar_function)(data,type);
}

/* Function to send a byte throug USART, I2C, SPI etc.*/
void Arduhdlc::sendchar(uint8_t data)
{
	(*this->sendchar_function)(data, 0);
}

void Arduhdlc::wrapPackage(const uint8_t * framebuffer, uint8_t frame_length)
{
}

void Arduhdlc::unwrapPackage(const uint8_t * framebuffer, uint8_t frame_length)
{
	
	// continue if packet has minimum required data
	// FB//DIR//CRC//FB
	// 1 // 1 // 2 // 1
	uint8_t minReqData = 4;
	if (frame_length > minReqData) {
		uint8_t datalength = frame_length - (uint8_t)CRC_LENGTH - (uint8_t)ADDRESS_LENGTH;

		uint8_t *data = (uint8_t *)malloc(datalength);

		for (int i = 1; i <= datalength; i++) {
			data[i - 1] = framebuffer[i];
			
		}
		
		(*frame_handler)(data, (uint8_t)(datalength), framebuffer[0]);
	}
}

/* Function to find valid HDLC frame from incoming data */
void Arduhdlc::charReceiver(uint8_t data)
{
	//Serial.println(data,HEX);
	/* FRAME FLAG */
	if (data == FRAME_BOUNDARY_OCTET)
	{
		if (this->escape_character == true)
		{	
			//Serial.println("TRUE");
			this->escape_character = false;
		}
		/* If a valid frame is detected */
		else if (
			(this->frame_position >= 2) &&
			(this->frame_checksum == (
				(this->receive_frame_buffer[this->frame_position - 1] << 8) | 
				(this->receive_frame_buffer[this->frame_position - 2] & 0xff)
				)
			)
		)  // (msb << 8 ) | (lsb & 0xff)
		{
			//Serial.println("ready");
			/* Call the user defined function and pass frame to it */
			//Serial.print("hola");
			unwrapPackage(receive_frame_buffer, this->frame_position);
		}
		this->frame_position = 0;
		this->frame_checksum = CRC16_CCITT_INIT_VAL;
		return;
	}

	if (this->escape_character)
	{
		this->escape_character = false;
		data ^= INVERT_OCTET;
	}
	else if (data == CONTROL_ESCAPE_OCTET)
	{
		this->escape_character = true;
		return;
	}

	receive_frame_buffer[this->frame_position] = data;
	// Calulate checksum if at least one byte of data in buffer
	if (this->frame_position - 2 >= 0) {
		this->frame_checksum = _crc_ccitt_update(this->frame_checksum, receive_frame_buffer[this->frame_position - 2]);
	}

	this->frame_position++;

	if (this->frame_position == this->max_frame_length)
	{
		this->frame_position = 0;
		this->frame_checksum = CRC16_CCITT_INIT_VAL;
	}
}

/* Wrap given data in HDLC frame and send it out byte at a time*/
void Arduhdlc::frameDecode(uint8_t *framebuffer, uint8_t frame_length, uint8_t receiver_address)
{
	uint8_t data;
	uint16_t fcs = CRC16_CCITT_INIT_VAL;

	this->sendchar((uint8_t)FRAME_BOUNDARY_OCTET);
	//Address direction
	fcs = _crc_ccitt_update(fcs, receiver_address);
	if ((receiver_address == CONTROL_ESCAPE_OCTET) || (receiver_address == FRAME_BOUNDARY_OCTET)) {
		this->sendchar((uint8_t)CONTROL_ESCAPE_OCTET);
		receiver_address ^= INVERT_OCTET;
	}
	this->sendchar(receiver_address);

	while (frame_length)
	{
		data = *framebuffer++;
		fcs = _crc_ccitt_update(fcs, data);
		if ((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
		{
			this->sendchar((uint8_t)CONTROL_ESCAPE_OCTET);
			data ^= INVERT_OCTET;
		}
		this->sendchar((uint8_t)data);
		frame_length--;
	}
	data = low(fcs);
	if ((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
	{
		this->sendchar((uint8_t)CONTROL_ESCAPE_OCTET);
		data ^= (uint8_t)INVERT_OCTET;
	}
	this->sendchar((uint8_t)data);
	data = high(fcs);
	if ((data == CONTROL_ESCAPE_OCTET) || (data == FRAME_BOUNDARY_OCTET))
	{
		this->sendchar(CONTROL_ESCAPE_OCTET);
		data ^= INVERT_OCTET;
	}
	this->sendchar(data);
	this->sendchar(FRAME_BOUNDARY_OCTET);
}


