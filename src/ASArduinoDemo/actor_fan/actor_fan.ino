/*
Name:		Sketch1.ino
Created:	19/05/2016 23:02:58
Author:	Trpzn
*/


#define DEBUG

#include "defined_commands.h"
#include <Arduhdlc.h>
#include <avr/boot.h>

/* How long (bytes) is the longest HDLC frame? */
#define MAX_HDLC_FRAME_LENGTH 32
//#define SERVO_PIN 14
#define LED_ON_PIN 7
#define FAN_PIN 5
#define BUTTON_PIN 1

#define VERSION_NUMBER 0.1
#define DEVICE_UNIQUE_ADDRESS 2
#define RECEIVER_UNIQUE_ADRESS 2
#define SERIAL_BAUD_RATE 115200

/* Variable for LED toggle */
boolean toggle = false;
uint8_t fan_speed = 0;

/* Function to send out byte/char */
void send_character(uint8_t data, uint8_t type);
/* Function to handle a valid HDLC frame */
void hdlc_command_router(const uint8_t *framebuffer, uint16_t framelength, uint8_t address);
void commands_router(hdlc_frame *frame);
void checkButtonPressed(void);

/* Command execute functions for each command */
void command_error(void);
void command_get_id(void);

/*Status of the device for automatic send data*/
int device_status = 0;

boolean buttonstatus = false;
unsigned long time;
unsigned long elapsedTime = 0;
unsigned long waitingTimeMS = 1;





/* Initialize Arduhdlc library with three parameters.
1. Character send function, to send out HDLC frame one byte at a time.
2. HDLC frame handler function for received frame.
3. Length of the longest frame used, to allocate buffer in memory */
Arduhdlc hdlc(&send_character, &hdlc_command_router, MAX_HDLC_FRAME_LENGTH, DEVICE_UNIQUE_ADDRESS);

/* Function to send out one 8bit character */
void send_character(uint8_t data, uint8_t type) {
	if (type == 0) {
		Serial.write(data);
		//Serial.print('_');
	}else {
		Serial.print(data, type);
	}
}


void command_toggle_led(const uint8_t *framebuffer, uint16_t framelengthd) {
	toggle = !toggle;
	digitalWrite(LED_ON_PIN, toggle);
}

// ECHO the data back, for testing CRC sum
void command_echo_data(uint8_t *framebuffer, uint16_t framelength) {
	hdlc.frameDecode(framebuffer, framelength, RECEIVER_UNIQUE_ADRESS);
}

void command_error(void) {
	uint8_t data[2] = { (uint8_t)RESPONSE_ERROR, 1 };
	data[0] = RESPONSE_ERROR;
	Serial.write("Error");
	hdlc.frameDecode(data, 2, RECEIVER_UNIQUE_ADRESS);
}

void command_default(void) {                                                                                                                                      
	Serial.write("default");
}

hdlc_frame *fillFrame(const uint8_t *framebuffer, uint16_t framelength) {
	hdlc_frame *frame = (hdlc_frame *)malloc(sizeof(hdlc_frame));
	/*Serial.print('\n');
	for (int x = 0; x < framelength; ++x) {
	Serial.print(framebuffer[x], HEX);
	Serial.write(' ');
	}
	Serial.print('\n');*/
	//return frame;
	frame->dataType = static_cast<serial_data_types>(framebuffer[0]);
	if (frame->dataType == DATA_TYPE_COMMAND) {
		frame->responseType = RESPONSE_ERROR;
		frame->commandType = static_cast<serial_commands>(framebuffer[1]);
	}
	else if (frame->dataType == DATA_TYPE_RESPONSE) {
		frame->responseType = static_cast<serial_responses>(framebuffer[1]);
		frame->commandType = COMMAND_ERROR;
	}
	frame->datalength = framelength - 2;
	frame->data = (uint8_t*)malloc(sizeof(uint8_t)*(framelength - 2));
	for (int x = 2; x < framelength; ++x) {
		frame->data[x - 2] = framebuffer[x];
	}
	return frame;
}


/*
* Command router/dispatcher
* Calls the right command, passing data to it.
* @TODO Make it better
* https://doanduyhai.wordpress.com/2012/08/04/design-pattern-the-asynchronous-dispatcher/
*/
void hdlc_command_router(const uint8_t *framebuffer, uint16_t framelength, uint8_t address) {
	//Serial.println("Hola");
	hdlc_frame *frame = fillFrame(framebuffer, framelength);
	//Serial.println(frame->dataType,HEX);
	//return;
	//enum serial_data_types datatype = static_cast<serial_data_types>(framebuffer[0]);
	switch (frame->dataType)
	{
	case DATA_TYPE_ERROR:  break;
	case DATA_TYPE_COMMAND: commands_router(frame); break;
	case DATA_TYPE_RESPONSE: responses_router(frame); break;
	default:
		delete frame;
		data_type_default();
		break;
	}

}

void data_type_default() {

}

void commands_router(hdlc_frame *frame) {
	//Serial.println(frame->commandType);
	//Serial.print(frame->commandType);
	switch (frame->commandType)
	{
		//case COMMAND_ERROR:                  command_error(); break;
	case COMMAND_GET_ID:				 command_get_id(); delete frame; break;
	case COMMAND_ONOFF_DEVICE:              command_onoff_device(frame); break;
	case COMMAND_SET_FAN_SPEED:              command_set_fan_speed(frame); break;
	default:
		delete frame;
		command_default();
		break;
	}
}

void command_get_id() {
	uint8_t data[3];
	data[0] = DATA_TYPE_RESPONSE;
	data[1] = RESPONSE_ECHO_ID; //Device Signature Byte 1
	data[2] = DEVICE_UNIQUE_ADDRESS; //Device Signature Byte 2
	hdlc.frameDecode(data, 3, RECEIVER_UNIQUE_ADRESS);
}

void command_onoff_device(hdlc_frame *frame) {
	//delete frame;
	//return;
	uint8_t data[3];
	//Serial.print('\n');
	/*
	for (int x = 0; x < frame->datalength; ++x) {
	Serial.print(frame->data[x], HEX);
	Serial.write('-');
	}
	*/
	//Serial.println(frame->data[0]);
	if (frame->data[0] == 1 && device_status == 0) {
		//turn on the device
		device_status = 1;
		digitalWrite(LED_ON_PIN, HIGH);
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 1;

	}
	else if (frame->data[0] == 0 && device_status == 1) {
		device_status = 0;
		digitalWrite(LED_ON_PIN, LOW);
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 0;
	}
	else {
		data[0] = DATA_TYPE_RESPONSE;
		data[1] = RESPONSE_ONOFF_DEVICE;
		data[2] = 2; //error

	}
	hdlc.frameDecode(data, 3, RECEIVER_UNIQUE_ADRESS);
	delete frame;
}

void command_set_fan_speed(hdlc_frame *frame) {
	
	uint8_t data[3];
	
	if (device_status == 0) {
		return;
	}
	else if (device_status == 1) {
		fan_speed = frame->data[0];
		Serial.print(fan_speed,HEX);
	}
	delete frame;
}

void responses_router(hdlc_frame *frame) {
	switch (frame->commandType)
	{
		//case COMMAND_ERROR:                  command_error(); break;
		//case COMMAND_GET_ID:				 command_get_id(framebuffer, framelength); break;
		//case COMMAND_TOGGLE_ON:       command_toggle_led(framebuffer, framelength); break;
	default:
		command_default();
		break;
	}
}

void setup() {
	//pinMode(1,OUTPUT); // Serial port TX to output
	//servo1.attach(SERVO_PIN); // Servo on analog pin A0
	//servo1.write(512);
	pinMode(LED_ON_PIN, OUTPUT);
	// initialize button reading
	pinMode(BUTTON_PIN, INPUT);
	pinMode(FAN_PIN, OUTPUT);
	// initialize serial port to 9600 baud
	Serial.begin(SERIAL_BAUD_RATE);

	//Serial.println("Ready");
	/*
	uint8_t data[] = { (uint8_t)1,(uint8_t)7,(uint8_t)0 };
	hdlc.frameDecode(data, sizeof(data), DEVICE_UNIQUE_ADDRESS);
	//data[] = { (uint8_t)2,(uint8_t)7,(uint8_t)255 };
	data[0] = (uint8_t)1;
	data[1] = (uint8_t)7;
	data[2] = (uint8_t)255;
	hdlc.frameDecode(data, sizeof(data), DEVICE_UNIQUE_ADDRESS);
	data[0] = (uint8_t)1;
	data[1] = (uint8_t)6;
	data[2] = (uint8_t)1;
	hdlc.frameDecode(data, sizeof(data), DEVICE_UNIQUE_ADDRESS);
	*/
}

void loop() {

	time = millis();
	if ((time - elapsedTime) >= waitingTimeMS) {
		checkButtonPressed();
		elapsedTime = time;
	}
	if (device_status == 1) {
		analogWrite(FAN_PIN, fan_speed);
		//abalogWri
	}

}



void checkButtonPressed() {
	if (device_status == 1 && digitalRead(BUTTON_PIN) && !buttonstatus) {
		buttonstatus = true;
		uint8_t data[] = { (uint8_t)2,(uint8_t)4,(uint8_t)1 };
		hdlc.frameDecode(data, sizeof(data), RECEIVER_UNIQUE_ADRESS);
	}
	else if (device_status == 1 && !digitalRead(BUTTON_PIN) && buttonstatus) {
		buttonstatus = false;
	}
}

/*
SerialEvent occurs whenever a new data comes in the
hardware serial RX.  This routine is run between each
time loop() runs, so using delay inside loop can delay
response.  Multiple bytes of data may be available.
*/
void serialEvent() {
	int incomingByte = 0;
	while (Serial.available()) {
		// get the new byte:
		//Serial.write();
		uint8_t inChar = (uint8_t)Serial.read();
		hdlc.charReceiver(inChar);
		//incomingByte = Serial.read();
		// say what you got:
		//Serial.print("I received: ");
		//Serial.println(incomingByte, HEX);
	}
}
