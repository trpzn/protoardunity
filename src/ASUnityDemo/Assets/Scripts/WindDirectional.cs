﻿using UnityEngine;
using System.Collections;

public class WindDirectional : MonoBehaviour {

	public ASarduini_SimpleFan simpleFan;
	private bool windStatus = false;

	Vector3 windLeft = new Vector3(0,0,1);
	Vector3 windRight = new Vector3(0,0,-1);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnTriggerExit(Collider other){
		if (other.attachedRigidbody) {
			if (windStatus) {
				simpleFan.setFan (false);
				windStatus = false;
			}
			Debug.Log ("sale");
		}



	}

	void OnTriggerStay(Collider other) {
		//Debug.Log("trigger");
		if (other.attachedRigidbody) {
			
			//Debug.Log ("Holas");
			//other.transform.Translate (windLeft * 0.1f);
			if (other.attachedRigidbody.velocity.Equals(Vector3.zero)) {
				other.attachedRigidbody.velocity = windLeft*0.5f;
			}
			if (other.attachedRigidbody.drag>0) {
				other.attachedRigidbody.AddForce (windLeft * 3, ForceMode.Impulse);
			} else {
				other.attachedRigidbody.AddForce (windLeft * (3/2), ForceMode.Impulse);
			}

			if (!windStatus) {
				Debug.Log ("entra");
				simpleFan.setFan (true);
				windStatus = true;
			}

		}
	}

}
