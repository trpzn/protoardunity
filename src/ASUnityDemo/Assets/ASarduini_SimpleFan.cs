﻿using UnityEngine;
using System.Collections;

public class ASarduini_SimpleFan : MonoBehaviour ,IAsarduinity {

	public int deviceId = 2;

	//bool ligthStatus = false;
	//bool currStatus = false;
	//bool currLigth = false;
	//float ligthTime = 0f;
	//float ligthTimeMax = 0.1f;

	//float distance = 10000f;
	//public float minDistance = 2f;
	//public float maxDistance = 10f;
	public bool fanStatus = false;
	int fanSpeed = 0;

	//public GameObject camara;
	//public Transform cameraTransform;

	public ASardunity asarduinity;
	// Use this for initialization
	void Start () {
		asarduinity = GameObject.FindObjectOfType<ASardunity> ();
		subscribeToHub (deviceId,this);
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void setFan(bool setTo){

		bool speedChanged = false;

		//Debug.Log (fanSpeed);
		if(fanStatus && !setTo){
			fanSpeed = 0;
			speedChanged = true;
		}else if(!fanStatus && setTo){
			fanSpeed = 255;
			speedChanged = true;
		}

		if(speedChanged){
			fanStatus = setTo;
			this.fanSpeed = fanSpeed;
			uint[] message = { (uint)1, (uint)7,(uint)fanSpeed};
			messageToHub (message);
			speedChanged = false;
		}
	}

	string toString(uint[] data){
		return data.ToString();
	}

	public void messageToHub (uint[] frame){
		asarduinity.SendMessageToHub (frame,deviceId);
	}
	public void messageToAS (uint[] frame){
		Debug.Log (frame[0]);
	}
	public void subscribeToHub (int deviceId, IAsarduinity device){
		asarduinity.subscribeDevice (deviceId,device);
	}

	public void OnApplicationQuit(){
		setFan (false);
	}
}
