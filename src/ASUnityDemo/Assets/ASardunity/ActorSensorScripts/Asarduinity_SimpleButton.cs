﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Asarduinity_SimpleButton : MonoBehaviour {

	public int deviceId = 1;

	bool ligthStatus = false;
	bool currStatus = false;
	bool currLigth = false;
	float ligthTime = 0f;
	float ligthTimeMax = 0.1f;

	public ASardunity asarduinity;
	// Use this for initialization
	void Start () {
		asarduinity = GameObject.FindObjectOfType<ASardunity> ();
		//asarduinity.subscribeDevice (deviceId,this);
	}
	
	// Update is called once per frame
	void Update () {
		if(ligthStatus==true){
			if (ligthTime < ligthTimeMax) {
				ligthTime += Time.deltaTime;
				if (!currStatus) {
					gameObject.GetComponent<MeshRenderer> ().material.color = Color.red;
					currStatus = true;
				}
			} else {
				ligthTime = 0f;
				currStatus = false;
				ligthStatus = false;
				gameObject.GetComponent<MeshRenderer> ().material.color = Color.white;
			}
		}
	}

	//Data comun

	public void incomingMessage(uint[] data){
		uint address = data [0];
		uint datatype = data [1];
		uint type = data [2];
		uint payload = data[3];
		Debug.Log ("click");
		//data.CopyTo(payload,3);
		/*
		Debug.Log (
			"Address: "+address+" "+
			"DataType: "+datatype+" "+
			"Type: "+type+" "+
			"Payload: "+payload
		);*/
		if (datatype == 2 && type == 4 && !ligthStatus) {
			ligthStatus = true;
		}
	}

	void sendMessage(uint[] data){
		//asarduinity.SendMessage ();
	}

	string toString(uint[] data){
		return data.ToString();
	}
}
