﻿using UnityEngine;
using System.Collections;

public class Asarduinity : MonoBehaviour,IAsarduinity {

	public int deviceId = 2;

	bool ligthStatus = false;
	bool currStatus = false;
	bool currLigth = false;
	float ligthTime = 0f;
	float ligthTimeMax = 0.1f;

	float distance = 10000f;
	public float minDistance = 2f;
	public float maxDistance = 10f;
	bool fanStatus = false;
	int fanSpeed = 0;

	//public GameObject camara;
	public Transform cameraTransform;

	public ASardunity asarduinity;
	// Use this for initialization
	void Start () {
		asarduinity = GameObject.FindObjectOfType<ASardunity> ();
		subscribeToHub (deviceId,this);
	}

	// Update is called once per frame
	void Update () {
		distance = Vector3.Distance (transform.position,cameraTransform.position);
		//Debug.Log (distance);
		if (distance < maxDistance) {
			fanStatus = true;
			int fanSpeed = (int) Remap (distance);
			Debug.Log ("Fanspeed: " + fanSpeed);
			setFanSpeed (fanSpeed);
		} else {
			if (fanStatus) {
				fanStatus = false;
				setFanSpeed (0);
			}
		}
	}
	void setFanSpeed(int fanSpeed){
		if (fanSpeed > 255)
			fanSpeed = 255;
		//Debug.Log (fanSpeed);
		if (fanSpeed != this.fanSpeed) {
			Debug.Log (this.fanSpeed);
			this.fanSpeed = fanSpeed;
			uint[] message = { (uint)1, (uint)7,(uint)fanSpeed};
			messageToHub (message);
		}

	}

	float Remap(float value){
		float from1 = maxDistance;
		float to1 = minDistance;
		float from2 = 0;
		float to2 = 255;
		return (value - from1) / (to1-from1) * (to2-from2)+from2;
		//return (value - from1) / (to1-from1) * (to2-from2)+from2;

	}


	string toString(uint[] data){
		return data.ToString();
	}

	public void messageToHub (uint[] frame){
		asarduinity.SendMessageToHub (frame,deviceId);
	}
	public void messageToAS (uint[] frame){
		Debug.Log (frame[0]);
	}
	public void subscribeToHub (int deviceId, IAsarduinity device){
		asarduinity.subscribeDevice (deviceId,device);
	}

	public void OnApplicationQuit(){
		setFanSpeed (0);
	}
}
