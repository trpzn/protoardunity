﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using System.Collections.Generic;



public class ASardunity : MonoBehaviour {

	HDLCnocontrol hdlc;
	SerialManager serial;

	//private List<Asarduinity_SimpleButton> devices = new List<Asarduinity_SimpleButton>();
	private Dictionary<int,IAsarduinity> devices = new Dictionary<int, IAsarduinity> (); 

	public string comPort = "COM8";
	// Use this for initialization
	void Start () {
		hdlc = new HDLCnocontrol (this,false);
		serial = new SerialManager (this,comPort);
		//string[] ports = SerialPort.GetPortNames ();
		//System.IO.Ports.SerialPort.GetPortNames ();
		string result = "";

	}

	
	// Update is called once per frame
	void Update () {
		
		int x = serial.read ();
		if (x >= 0) {
			//Debug.Log (x);
			hdlc.charReceiver ((uint)x);
		}

	}


	public void subscribeDevice(int device, IAsarduinity instance){
		devices.Add (device, instance);
	}

	public void sendCharFunction (byte data){
		//Debug.Log (data);
		serial.write (data);
	}

	public void frameHandler(uint[] frame,uint address){
		//Debug.Log (frame.Length);
		if (devices.ContainsKey((int)address)) {
			devices [(int)address].messageToAS (frame);
		}

	}

	public void OnApplicationQuit(){
		serial.Stop ();
	}

	public void SendMessageToHub(uint[] frame, int address){
		//Debug.Log (frame[0].ToString()+" "+frame[1].ToString()+" "+frame[2].ToString());
		hdlc.frameDecode(frame,(uint)address);
	}
		
}

public interface IAsarduinity{
	void messageToHub (uint[] frame);
	void messageToAS (uint[] frame);
	void subscribeToHub (int deviceId, IAsarduinity device);
}