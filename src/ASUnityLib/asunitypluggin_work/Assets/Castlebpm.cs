﻿using UnityEngine;
using System.Collections;

public class Castlebpm : MonoBehaviour {

	public Transform castle1;
	public Transform castle2;
	public Asarduinity_bpm asardubpm;

	int currentBpm = 50;

	int cast1direction = 1;
	int cast2direction = 1;

	float inity = 0;
	float endingy = 25;


	// Use this for initialization
	void Start () {
		inity = castle1.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		Move (castle1,ref cast1direction);
		Move (castle2,ref cast2direction);
	}

	void Move(Transform castle,ref int direction){
		//int min = inity;
		//int max = endingy;
		//Mathf.Lerp(0, 5, Time.time), 0, .5);
		float speed = ((asardubpm.bpm)/(endingy - inity))/2; //metros en un segundo
		Vector3 translateVector = new Vector3(0,speed,0);
		if (direction == -1) {
			translateVector = new Vector3 (0,-speed,0);
		}

		translateVector = (castle.position + translateVector);

		if (translateVector.y > endingy) {
			//Debug.Log ("down");
			direction = -1;
			return;
		} else if (translateVector.y < inity) {
			//Debug.Log ("up");
			direction = 1;
			return;
		}

		//Debug.Log ();
		castle.position = translateVector;



	}



}
