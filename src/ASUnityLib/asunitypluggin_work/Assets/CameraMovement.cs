﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public float movement = 0.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.UpArrow) && transform.position.z<33) {
			transform.Translate (new Vector3(0,0,movement));
		}
		if (Input.GetKeyDown (KeyCode.DownArrow) && transform.position.z>-27) {
			transform.Translate (new Vector3(0,0,-movement));
		}
	
	}
}
