﻿using UnityEngine;
using System.Collections;
using System.IO.Ports;
using UnityEngine.UI;
using System.Threading;


public class SerialManager{
	public string serialportWindows;
	public string serialportLinux;
	string inputBuffer; 
	bool conected = false;
	float internalClock = 0;
	public float readEveryXMilisecs = 10;
	SerialPort mySerial;
	bool readRunning = true;

	private Queue writeBuffer = new Queue ();
	private Queue readBuffer = new Queue ();
	private ReadSerial readSerial;
	private Thread readSerialWorker;

	ASardunity asarduinity;

	public SerialManager(ASardunity asarduinity,string com){
		serialportWindows = com;
		this.asarduinity = asarduinity;
		conectar ();
	}

	public int read(){
		if (readBuffer.Count > 0) {
			return (int)readBuffer.Dequeue ();
		} else {
			return -1;
		}
	}

	public void conectar(){
		if (conected)
			return;
		mySerial = new SerialPort ();
		//Debug.Log (serialportWindows);
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
		mySerial.PortName = serialportWindows;
		#elif UNITY_EDITOR_LINUX || UNITY_STANDALONE_LINUX
		mySerial.PortName = serialportLinux;
		#endif

		Debug.Log (mySerial.PortName);
		mySerial.BaudRate = 256000;
		mySerial.ReadTimeout = 1;
		mySerial.ReadBufferSize = 1024;
		//mySerial.Open ();
		try
		{
			
			mySerial.Open();//Open the Port
			conected = true;
			mySerial.DiscardInBuffer();
			Debug.Log("Serial Port {0} Opened" + mySerial.PortName);
			readSerial = new ReadSerial(mySerial,readBuffer);
			readSerialWorker = new Thread(readSerial.DoWork);
			readSerialWorker.Start();

			//StartCoroutine("ReadFromSerial");


			//text.GetComponent<Text>()
			//text.text = "Serial Port {0} Opened" + mySerial.PortName;

		}    
		catch
		{
			//text.text = ;
			Debug.Log("ERROR in Opening Serial Port \n Corra como root");
		}  
	}


	void OnApplicationQuit() {
		//readRunning = false;
		//mySerial.Write ("0");
		mySerial.Close ();
	}
	void readData(){
		if (conected) {
			if (internalClock < (readEveryXMilisecs/1000)) {
				internalClock += Time.deltaTime;
				return;
			}
			//Debug.Log (1);
			//Debug.Log (internalClock);
			internalClock = 0;

		}
	}
	/*
	public void sendData(uint[] frame){
		foreach(int index in frame){
			uint current = frame [index];
			Debug.Log (current);
		}
		//Debug.Log (value);
		//mySerial.WriteLine (value.ToString());
	}
	*/

	public void write(byte data){
		Debug.Log (data);
		byte[] x = { data };
		try{
			mySerial.Write (x,0,1);
		}catch{

		}
		//mySerial.write
	}
	void dataReceived(){
		//Debug.Log (inputBuffer);
		//gameManager.dataReceivedFromSerial (inputBuffer);
		inputBuffer = "";
	}
	public void readEnqueue(int data){
		readBuffer.Enqueue (data);
	}

	public static IEnumerator WaitForRealSeconds(float time){
		float start = Time.realtimeSinceStartup;
		while(Time.realtimeSinceStartup<start + time){
			yield return null;
		}
	}

	public class ReadSerial{
		private volatile bool _shouldStop;
		//SerialManager serial;
		SerialPort mySerial;
		Queue readBuffer;
		public ReadSerial(SerialPort mySerial,Queue readBuffer){
			this.mySerial = mySerial;
			this.readBuffer = readBuffer;
		}
		public void DoWork()
		{
			while(!_shouldStop){
				try{
					int x = mySerial.ReadByte();
					//Debug.Log(x);
					readBuffer.Enqueue(x);

				}catch{
					//Debug.Log ("error");
				}
			}
		}

		public void RequestStop()
		{
			_shouldStop = true;
		}

	}

	public void Stop(){
		readSerial.RequestStop();
		readSerialWorker.Join ();
		mySerial.Close ();
		Debug.Log ("WellClosed");
	}

}
