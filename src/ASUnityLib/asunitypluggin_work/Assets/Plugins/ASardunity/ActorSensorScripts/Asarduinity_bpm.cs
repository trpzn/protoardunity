﻿using UnityEngine;
using System.Collections;

public class Asarduinity_bpm : MonoBehaviour,IAsarduinity {

	public int deviceId = 3;
	public int bpm = 0;

	float timeWithoutBeats = 0;
	public float waitTimeForBeats = 4; // time in ms

	public ASardunity asarduinity;
	// Use this for initialization
	void Start () {
		asarduinity = GameObject.FindObjectOfType<ASardunity> ();
		subscribeToHub (deviceId,this);
	}
	
	// Update is called once per frame
	void Update () {
		timeWithoutBeats += Time.deltaTime;
		if (timeWithoutBeats >= waitTimeForBeats && bpm !=0) {
			bpm = 0;
		}
	
	}

	string toString(uint[] data){
		return data.ToString();
	}

	public void messageToHub (uint[] frame){
		asarduinity.SendMessageToHub (frame,deviceId);
	}
	public void messageToAS (uint[] frame){
		//Debug.Log (frame[3].ToString());
		Debug.Log (frameToSting(frame));
		if(frame[1]==2 && frame[2]==4){
			bpm = (int)frame [3];
			if (bpm != 0)
				timeWithoutBeats = 0;
		}
	}
	public void subscribeToHub (int deviceId, IAsarduinity device){
		asarduinity.subscribeDevice (deviceId,device);
	}

	string frameToSting(uint[] frame){
		string output = "";
		if (frame.Length > 0) {
			output += "[";
		} else {
			return output;
		}
		foreach (uint data in frame) {
			output += data + ",";
		}
		output += "]";
		return output;
	}
}
